function findingAgender(genderData) {
  let aGender = genderData.filter((allGender) => {
    return allGender.gender === "Agender";
  });
  return aGender;
}
module.exports = findingAgender;
