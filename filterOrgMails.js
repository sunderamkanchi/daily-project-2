function filterOrgMails(employeeMails) {
  let filteredEmployeemails = employeeMails.filter((mails) => {
    return mails.email.endsWith("org");
  });
  return filteredEmployeemails;
}
module.exports = filterOrgMails;
