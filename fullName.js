function fullName(employeeName) {
  let employeeFullName = employeeName.map((addFullName) => {
    addFullName["fullName"] =
      addFullName.first_name + " " + addFullName.last_name;
    return addFullName;
  });
  return employeeFullName;
}
module.exports = fullName;
