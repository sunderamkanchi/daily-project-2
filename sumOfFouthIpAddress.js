function sumOfFourthIpAddress(ipAddress, cb) {
  let arrayIpAddress = cb(ipAddress);
  let secondIpAddresssum = arrayIpAddress.reduce((acc, curr) => {
    acc += curr.newIpAddress[3];
    return acc;
  }, 0);
  return secondIpAddresssum;
}
module.exports = sumOfFourthIpAddress;
