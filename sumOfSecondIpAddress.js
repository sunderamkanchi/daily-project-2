function sumofSecondIpAddress(ipAddress, cb) {
  let arrayIpAddress = cb(ipAddress);
  let secondIpAddresssum = arrayIpAddress.reduce((acc, curr) => {
    acc += curr.newIpAddress[1];
    return acc;
  }, 0);
  return secondIpAddresssum;
}
module.exports = sumofSecondIpAddress;
