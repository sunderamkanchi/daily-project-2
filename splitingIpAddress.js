function splitingIpAddress(ipAddress) {
  let finalIpAddress = ipAddress.map((changedIpAddress) => {
    let changes = changedIpAddress.ip_address.split(".");

    let changeIntoArray = changes.map((data) => parseInt(data));
    changedIpAddress["newIpAddress"] = changeIntoArray;
    return changedIpAddress;
  });

  return finalIpAddress;
}
module.exports = splitingIpAddress;
